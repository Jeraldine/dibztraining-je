/*
function count(add){
	var counter = document.getElementById('count').value;
	var a = parseInt(counter,10) + add;

	document.getElementById('count').value = a;
	return a;	
}
*/


$(function() {
    $('.btn-click').on('click', function() {
        var _this = $(this),
            _count = _this.closest('.kitty').find('.count'),
            _add = _count.text();

        /* increment */
        _count.html(_this.hasClass('btn-click') ? ++_add : 0);
    });
});